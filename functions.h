#ifndef HW6_1_FUNCTIONS_H
#define HW6_1_FUNCTIONS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "data.h"
#include "hash.h"


void validation(int argc);
Input_t parse_input_data(char *argv[]);
void find_and_print(Input_t input);

#endif
