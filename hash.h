#ifndef HW6_1_HASH_H
#define HW6_1_HASH_H

#include <stdlib.h>
#include <string.h>
#include "data.h"


typedef struct SubString_t {
    char *string;
    short number;
    struct SubString_t *next_p;
} SubString_t;

SubString_t *make_dict(Input_t input);
short add_or_pp(char string[], short zero_ch, SubString_t *head_p, short sub_len);
short sub_equals(const char *string_p, const char string[], short zero_ch, short sub_len);

#endif
