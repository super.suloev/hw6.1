#include "hash.h"

SubString_t *make_dict(Input_t input) {
    SubString_t *head_p;
    head_p = (SubString_t*) malloc(1 * sizeof(SubString_t));
    head_p->next_p = NULL;
    char *first_sub_p;
    first_sub_p = (char*) malloc(input.sub_len * sizeof(char));
    for (short i = 0; i < input.sub_len; i++)
        first_sub_p[i] = input.string[i];
    head_p->string = first_sub_p;
    head_p->number = 1;

    short len = strlen(input.string);

    for (short zero_ch = 1; zero_ch < len - 3; zero_ch++) {
        add_or_pp(input.string, zero_ch, head_p, input.sub_len);
    }

    return head_p;
}



short add_or_pp(char string[], short zero_ch, SubString_t *head_p, short sub_len) {
    SubString_t *thisItem_p;
    thisItem_p = head_p;
    while (1) {
        if (sub_equals(thisItem_p->string, string, zero_ch, sub_len)) {
            thisItem_p->number++;
            return 0;
        }

        if (thisItem_p->next_p != NULL)
            thisItem_p = thisItem_p->next_p;

        else {
            SubString_t *new_item_p;
            new_item_p = (SubString_t*) malloc(1 * sizeof(SubString_t));
            char *new_sub;
            new_sub = (char*) malloc(sub_len * sizeof(char));
            for (short i = 0; i < sub_len; i++)
                new_sub[i] = string[zero_ch + i];
            new_item_p->string = new_sub;
            new_item_p->number = 1;
            new_item_p->next_p = NULL;
            thisItem_p->next_p = new_item_p;
            return 0;
        }
    }
}

short sub_equals(const char *string_p, const char string[], short zero_ch, short sub_len) {
    for (short i = 0; i < sub_len; i++) {
        if (string_p[i] != string[zero_ch + i])
            return 0;
    }
    return 1;
}