#include "functions.h"

void validation(int argc) {
    if (argc != 4) {
        printf("Arguments number bust be equals three!");
        exit(1);
    }
}

Input_t parse_input_data(char *argv[]) {
    Input_t input;
    short i;
    for (i = 0; argv[1][i] != '\0'; i++)
        input.string[i] = argv[1][i];
    input.string[++i] = '\0';
    input.sub_len = atoi(argv[2]);
    input.output_rep = atoi(argv[3]);
    return input;
}

void find_and_print(Input_t input) {
    SubString_t *head_p = make_dict(input);

    SubString_t *i;
    i = head_p;
    while (1) {
        if (i->number >= input.output_rep) {
            printf("Substring - %s, Repeat count - %d \n", i->string, i->number);
        }
        if (i->next_p != NULL)
            i = i->next_p;
        else
            break;
    }
}