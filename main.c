#include "main.h"

int main(int argc, char *argv[]) {
    validation(argc);
    Input_t input = parse_input_data(argv);
    find_and_print(input);
    return 0;
}
